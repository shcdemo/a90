<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A90113">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>Honoured Sir, that the works of mercy and charity are the fruits of piety, is the undoubted tenent of all Christians, ...</title>
    <author>Odling, Edward.</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A90113 of text R212130 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Thomason 669.f.19[48]). Textual changes  and metadata enrichments aim at making the text more  computationally tractable, easier to read, and suitable for network-based collaborative curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in a standardized format that preserves archaic forms ('loveth', 'seekest'). Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2017</date>
    <idno type="DLPS">A90113</idno>
    <idno type="STC">Wing O139</idno>
    <idno type="STC">Thomason 669.f.19[48]</idno>
    <idno type="STC">ESTC R212130</idno>
    <idno type="EEBO-CITATION">99870777</idno>
    <idno type="PROQUEST">99870777</idno>
    <idno type="VID">163385</idno>
    <idno type="PROQUESTGOID">2240955790</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A90113)</note>
    <note>Transcribed from: (Early English Books Online ; image set 163385)</note>
    <note>Images scanned from microfilm: (Thomason Tracts ; 246:669f19[48])</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>Honoured Sir, that the works of mercy and charity are the fruits of piety, is the undoubted tenent of all Christians, ...</title>
      <author>Odling, Edward.</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>s.n.,</publisher>
      <pubPlace>[London :</pubPlace>
      <date>1654]</date>
     </publicationStmt>
     <notesStmt>
      <note>Signed at end: Edward Odling, iatrʹos, Solicitor for the said Corporation.</note>
      <note>Title from opening lines of text.</note>
      <note>Imprint from Wing.</note>
      <note>Annotation on Thomason copy: "December 1654".</note>
      <note>Reproduction of the original in the British Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Governors for the Poor (London, England) -- Early works to 1800.</term>
     <term>Poor -- England -- Early works to 1800.</term>
     <term>Charity -- Early works to 1800.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:tcp>A90113</ep:tcp>
    <ep:estc> R212130</ep:estc>
    <ep:stc> (Thomason 669.f.19[48]). </ep:stc>
    <ep:corpus>civilwar</ep:corpus>
    <ep:workpart>no</ep:workpart>
    <ep:title>Honoured Sir, that the works of mercy and charity are the fruits of piety, is the undoubted tenent of all Christians, ...</ep:title>
    <ep:author>Odling, Edward</ep:author>
    <ep:publicationYear>1654</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>264</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change><date>2007-07</date><label>TCP</label>
        Assigned for keying and markup
      </change>
   <change><date>2007-07</date><label>Aptara</label>
        Keyed and coded from ProQuest page images
      </change>
   <change><date>2007-08</date><label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change><date>2007-08</date><label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change><date>2008-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A90113-e10">
  <body xml:id="A90113-e20">
   <pb facs="tcp:163385:1" rend="simple:additions" xml:id="A90113-001-a"/>
   <div type="letter" xml:id="A90113-e30">
    <opener xml:id="A90113-e40">
     <salute xml:id="A90113-e50">
      <w lemma="honour" pos="j-vn" xml:id="A90113-001-a-0010">Honoured</w>
      <w lemma="sir" pos="n1" xml:id="A90113-001-a-0020">Sir</w>
      <pc xml:id="A90113-001-a-0030">,</pc>
     </salute>
    </opener>
    <p xml:id="A90113-e60">
     <w lemma="that" pos="cs" xml:id="A90113-001-a-0040">THat</w>
     <w lemma="the" pos="d" xml:id="A90113-001-a-0050">the</w>
     <w lemma="work" pos="n2" xml:id="A90113-001-a-0060">works</w>
     <w lemma="of" pos="acp" xml:id="A90113-001-a-0070">of</w>
     <w lemma="mercy" pos="n1" xml:id="A90113-001-a-0080">Mercy</w>
     <w lemma="and" pos="cc" xml:id="A90113-001-a-0090">and</w>
     <w lemma="charity" pos="n1" xml:id="A90113-001-a-0100">Charity</w>
     <w lemma="be" pos="vvb" xml:id="A90113-001-a-0110">are</w>
     <w lemma="the" pos="d" xml:id="A90113-001-a-0120">the</w>
     <w lemma="fruit" pos="n2" xml:id="A90113-001-a-0130">fruits</w>
     <w lemma="of" pos="acp" xml:id="A90113-001-a-0140">of</w>
     <w lemma="piety" pos="n1" xml:id="A90113-001-a-0150">Piety</w>
     <pc xml:id="A90113-001-a-0160">,</pc>
     <w lemma="be" pos="vvz" xml:id="A90113-001-a-0170">is</w>
     <w lemma="the" pos="d" xml:id="A90113-001-a-0180">the</w>
     <w lemma="undoubted" pos="j" xml:id="A90113-001-a-0190">undoubted</w>
     <w lemma="tenent" pos="n1" xml:id="A90113-001-a-0200">Tenent</w>
     <w lemma="of" pos="acp" xml:id="A90113-001-a-0210">of</w>
     <w lemma="all" pos="d" xml:id="A90113-001-a-0220">all</w>
     <w lemma="christian" pos="nn2" xml:id="A90113-001-a-0230">Christians</w>
     <pc xml:id="A90113-001-a-0240">,</pc>
     <w lemma="ground" pos="vvn" xml:id="A90113-001-a-0250">grounded</w>
     <w lemma="upon" pos="acp" xml:id="A90113-001-a-0260">upon</w>
     <w lemma="the" pos="d" xml:id="A90113-001-a-0270">the</w>
     <w lemma="clear" pos="j" xml:id="A90113-001-a-0280">clear</w>
     <w lemma="proof" pos="n2" xml:id="A90113-001-a-0290">proofs</w>
     <w lemma="of" pos="acp" xml:id="A90113-001-a-0300">of</w>
     <w lemma="scripture" pos="n1" xml:id="A90113-001-a-0310">Scripture</w>
     <pc xml:id="A90113-001-a-0320">:</pc>
     <w lemma="58" pos="crd" xml:id="A90113-001-a-0330">58</w>
     <hi xml:id="A90113-e70">
      <w lemma="be" pos="vvz" xml:id="A90113-001-a-0340">Is</w>
      <pc xml:id="A90113-001-a-0350">.</pc>
     </hi>
     <w lemma="the" pos="d" xml:id="A90113-001-a-0360">the</w>
     <w lemma="whole" pos="j" xml:id="A90113-001-a-0370">whole</w>
     <w lemma="chap." pos="ab" xml:id="A90113-001-a-0380">chap.</w>
     <pc unit="sentence" xml:id="A90113-001-a-0390"/>
    </p>
    <p xml:id="A90113-e80">
     <w lemma="and" pos="cc" xml:id="A90113-001-a-0400">And</w>
     <w lemma="that" pos="cs" xml:id="A90113-001-a-0410">that</w>
     <w lemma="the" pos="d" xml:id="A90113-001-a-0420">the</w>
     <w lemma="fruit" pos="n2" xml:id="A90113-001-a-0430">fruits</w>
     <w lemma="of" pos="acp" xml:id="A90113-001-a-0440">of</w>
     <w lemma="piety" pos="n1" xml:id="A90113-001-a-0450">Piety</w>
     <w lemma="shall" pos="vmd" xml:id="A90113-001-a-0460">should</w>
     <w lemma="be" pos="vvi" xml:id="A90113-001-a-0470">be</w>
     <w lemma="always" pos="av" xml:id="A90113-001-a-0480">always</w>
     <w lemma="fresh" pos="j" xml:id="A90113-001-a-0490">fresh</w>
     <w lemma="and" pos="cc" xml:id="A90113-001-a-0500">and</w>
     <w lemma="flourish" pos="j-vg" xml:id="A90113-001-a-0510">flourishing</w>
     <pc xml:id="A90113-001-a-0520">,</pc>
     <w lemma="be" pos="vvz" xml:id="A90113-001-a-0530">is</w>
     <w lemma="as" pos="acp" xml:id="A90113-001-a-0540">as</w>
     <w lemma="clear" pos="av-j" xml:id="A90113-001-a-0550">clearly</w>
     <w lemma="evidence" pos="vvn" xml:id="A90113-001-a-0560">evidenced</w>
     <w lemma="in" pos="acp" xml:id="A90113-001-a-0570">in</w>
     <w lemma="holy" pos="j" xml:id="A90113-001-a-0580">holy</w>
     <w lemma="writ" pos="n1" xml:id="A90113-001-a-0590">writ</w>
     <pc xml:id="A90113-001-a-0600">:</pc>
     <w lemma="92" pos="crd" xml:id="A90113-001-a-0610">92</w>
     <hi xml:id="A90113-e90">
      <w lemma="ps." pos="ab" xml:id="A90113-001-a-0620">Ps.</w>
     </hi>
     <w lemma="12" pos="crd" xml:id="A90113-001-a-0640">12</w>
     <pc xml:id="A90113-001-a-0650">,</pc>
     <w lemma="13" pos="crd" xml:id="A90113-001-a-0660">13</w>
     <pc xml:id="A90113-001-a-0670">,</pc>
     <w lemma="14." pos="crd" xml:id="A90113-001-a-0680">14.</w>
     <pc unit="sentence" xml:id="A90113-001-a-0690"/>
    </p>
    <p xml:id="A90113-e100">
     <w lemma="and" pos="cc" xml:id="A90113-001-a-0700">And</w>
     <w lemma="then" pos="av" xml:id="A90113-001-a-0710">then</w>
     <w lemma="most" pos="avs-d" xml:id="A90113-001-a-0720">most</w>
     <w lemma="especial" pos="av-j" xml:id="A90113-001-a-0730">especially</w>
     <pc xml:id="A90113-001-a-0740">,</pc>
     <w lemma="when" pos="crq" xml:id="A90113-001-a-0750">when</w>
     <w lemma="the" pos="d" xml:id="A90113-001-a-0760">the</w>
     <w lemma="lord" pos="n1" xml:id="A90113-001-a-0770">Lord</w>
     <w lemma="God" pos="nn1" xml:id="A90113-001-a-0780">God</w>
     <w lemma="have" pos="vvz" xml:id="A90113-001-a-0790">hath</w>
     <w lemma="be" pos="vvn" xml:id="A90113-001-a-0800">been</w>
     <w lemma="see" pos="vvn" xml:id="A90113-001-a-0810">seen</w>
     <w lemma="in" pos="acp" xml:id="A90113-001-a-0820">in</w>
     <w lemma="the" pos="d" xml:id="A90113-001-a-0830">the</w>
     <w lemma="mount" pos="n1" xml:id="A90113-001-a-0840">Mount</w>
     <pc xml:id="A90113-001-a-0850">,</pc>
     <w lemma="in" pos="acp" xml:id="A90113-001-a-0860">in</w>
     <w lemma="the" pos="d" xml:id="A90113-001-a-0870">the</w>
     <w lemma="time" pos="n1" xml:id="A90113-001-a-0880">time</w>
     <w lemma="of" pos="acp" xml:id="A90113-001-a-0890">of</w>
     <w lemma="eminent" pos="j" xml:id="A90113-001-a-0900">Eminent</w>
     <w lemma="and" pos="cc" xml:id="A90113-001-a-0910">and</w>
     <w lemma="imminent" pos="j" xml:id="A90113-001-a-0920">Imminent</w>
     <w lemma="danger" pos="n1" xml:id="A90113-001-a-0930">danger</w>
     <w lemma="and" pos="cc" xml:id="A90113-001-a-0940">and</w>
     <w lemma="distress" pos="n1" reg="distress" xml:id="A90113-001-a-0950">distresse</w>
     <pc xml:id="A90113-001-a-0960">;</pc>
     <w lemma="and" pos="cc" xml:id="A90113-001-a-0970">and</w>
     <w lemma="have" pos="vvz" xml:id="A90113-001-a-0980">hath</w>
     <w lemma="give" pos="vvn" xml:id="A90113-001-a-0990">given</w>
     <w lemma="signal" pos="n1" reg="signal" xml:id="A90113-001-a-1000">signall</w>
     <w lemma="deliverance" pos="n2" xml:id="A90113-001-a-1010">deliverances</w>
     <pc xml:id="A90113-001-a-1020">,</pc>
     <w lemma="by" pos="acp" xml:id="A90113-001-a-1030">by</w>
     <w lemma="imparalleld" pos="j" xml:id="A90113-001-a-1040">imparalleld</w>
     <w lemma="victory" pos="n2" xml:id="A90113-001-a-1050">Victories</w>
     <w lemma="both" pos="av-d" xml:id="A90113-001-a-1060">both</w>
     <w lemma="by" pos="acp" xml:id="A90113-001-a-1070">by</w>
     <w lemma="sea" pos="n1" xml:id="A90113-001-a-1080">Sea</w>
     <w lemma="and" pos="cc" xml:id="A90113-001-a-1090">and</w>
     <w lemma="land" pos="n1" xml:id="A90113-001-a-1100">Land</w>
     <pc xml:id="A90113-001-a-1110">:</pc>
     <w lemma="68" pos="crd" xml:id="A90113-001-a-1120">68</w>
     <foreign xml:id="A90113-e110" xml:lang="lat">
      <w lemma="ps." pos="ab" xml:id="A90113-001-a-1130">Ps.</w>
      <w lemma="per" pos="fla" xml:id="A90113-001-a-1150">per</w>
      <w lemma="tot" pos="fla" xml:id="A90113-001-a-1160">tot</w>
      <pc unit="sentence" xml:id="A90113-001-a-1170">.</pc>
     </foreign>
    </p>
    <p xml:id="A90113-e120">
     <w lemma="therefore" pos="av" xml:id="A90113-001-a-1180">Therefore</w>
     <w lemma="you" pos="pn" xml:id="A90113-001-a-1190">you</w>
     <w lemma="be" pos="vvb" xml:id="A90113-001-a-1200">are</w>
     <w lemma="most" pos="avs-d" xml:id="A90113-001-a-1210">most</w>
     <w lemma="humble" pos="av-j" xml:id="A90113-001-a-1220">humbly</w>
     <pc xml:id="A90113-001-a-1230">,</pc>
     <w lemma="and" pos="cc" xml:id="A90113-001-a-1240">and</w>
     <w lemma="earnest" pos="av-j" xml:id="A90113-001-a-1250">earnestly</w>
     <w lemma="entreat" pos="vvn" reg="entreated" xml:id="A90113-001-a-1260">intreated</w>
     <pc xml:id="A90113-001-a-1270">,</pc>
     <w lemma="that" pos="cs" xml:id="A90113-001-a-1280">that</w>
     <w lemma="you" pos="pn" xml:id="A90113-001-a-1290">you</w>
     <w lemma="will" pos="vmd" xml:id="A90113-001-a-1300">would</w>
     <w lemma="be" pos="vvi" xml:id="A90113-001-a-1310">be</w>
     <w lemma="please" pos="vvn" xml:id="A90113-001-a-1320">pleased</w>
     <pc xml:id="A90113-001-a-1330">,</pc>
     <w lemma="to" pos="prt" xml:id="A90113-001-a-1340">to</w>
     <w lemma="call" pos="vvi" xml:id="A90113-001-a-1350">call</w>
     <w lemma="upon" pos="acp" xml:id="A90113-001-a-1360">upon</w>
     <w lemma="the" pos="d" xml:id="A90113-001-a-1370">the</w>
     <w lemma="grand" pos="j" xml:id="A90113-001-a-1380">Grand</w>
     <w lemma="act" pos="n1" xml:id="A90113-001-a-1390">Act</w>
     <w lemma="for" pos="acp" xml:id="A90113-001-a-1400">for</w>
     <w lemma="employ" pos="vvg" reg="employing" xml:id="A90113-001-a-1410">imploying</w>
     <pc xml:id="A90113-001-a-1420">,</pc>
     <w lemma="and" pos="cc" xml:id="A90113-001-a-1430">and</w>
     <w lemma="relieve" pos="vvg" reg="relieving" xml:id="A90113-001-a-1440">releiving</w>
     <w lemma="the" pos="d" xml:id="A90113-001-a-1450">the</w>
     <w lemma="poor" pos="j" xml:id="A90113-001-a-1460">Poor</w>
     <w lemma="of" pos="acp" xml:id="A90113-001-a-1470">of</w>
     <w lemma="the" pos="d" xml:id="A90113-001-a-1480">the</w>
     <w lemma="whole" pos="j" xml:id="A90113-001-a-1490">whole</w>
     <w lemma="nation" pos="n1" xml:id="A90113-001-a-1500">Nation</w>
     <pc xml:id="A90113-001-a-1510">:</pc>
     <w lemma="which" pos="crq" xml:id="A90113-001-a-1520">which</w>
     <pc join="right" xml:id="A90113-001-a-1530">(</pc>
     <w lemma="after" pos="acp" xml:id="A90113-001-a-1540">after</w>
     <w lemma="ten" pos="crd" xml:id="A90113-001-a-1550">ten</w>
     <w lemma="month" pos="ng2" reg="months'" xml:id="A90113-001-a-1560">moneths</w>
     <w lemma="conception" pos="n1" xml:id="A90113-001-a-1570">conception</w>
     <pc xml:id="A90113-001-a-1580">)</pc>
     <w lemma="for" pos="acp" xml:id="A90113-001-a-1590">for</w>
     <w lemma="some" pos="d" xml:id="A90113-001-a-1600">some</w>
     <w lemma="week" pos="n2" xml:id="A90113-001-a-1610">weeks</w>
     <w lemma="have" pos="vvz" xml:id="A90113-001-a-1620">hath</w>
     <w lemma="be" pos="vvn" xml:id="A90113-001-a-1630">been</w>
     <w lemma="dormant" pos="j" xml:id="A90113-001-a-1640">dormant</w>
     <pc join="right" xml:id="A90113-001-a-1650">(</pc>
     <w lemma="in" pos="acp" xml:id="A90113-001-a-1660">in</w>
     <w lemma="the" pos="d" xml:id="A90113-001-a-1670">the</w>
     <w lemma="birth" pos="n1" xml:id="A90113-001-a-1680">Birth</w>
     <pc xml:id="A90113-001-a-1690">)</pc>
     <w lemma="upon" pos="acp" xml:id="A90113-001-a-1700">upon</w>
     <w lemma="the" pos="d" xml:id="A90113-001-a-1710">the</w>
     <w lemma="ingrossement" pos="n1" xml:id="A90113-001-a-1720">ingrossement</w>
     <pc xml:id="A90113-001-a-1730">;</pc>
     <w lemma="and" pos="cc" xml:id="A90113-001-a-1740">And</w>
     <w lemma="without" pos="acp" xml:id="A90113-001-a-1750">without</w>
     <w lemma="your" pos="po" xml:id="A90113-001-a-1760">your</w>
     <w lemma="assistance" pos="n1" xml:id="A90113-001-a-1770">assistance</w>
     <w lemma="may" pos="vmb" xml:id="A90113-001-a-1780">may</w>
     <w lemma="prove" pos="vvi" xml:id="A90113-001-a-1790">prove</w>
     <w lemma="abortive" pos="j" xml:id="A90113-001-a-1800">abortive</w>
     <pc xml:id="A90113-001-a-1810">:</pc>
     <w lemma="together" pos="av" xml:id="A90113-001-a-1820">Together</w>
     <w lemma="with" pos="acp" xml:id="A90113-001-a-1830">with</w>
     <w lemma="the" pos="d" xml:id="A90113-001-a-1840">the</w>
     <w lemma="afterbirth" pos="n1" reg="afterbirth" xml:id="A90113-001-a-1850">After-birth</w>
     <w lemma="thereof" pos="av" xml:id="A90113-001-a-1860">thereof</w>
     <pc xml:id="A90113-001-a-1870">;</pc>
     <w lemma="a" pos="d" xml:id="A90113-001-a-1880">An</w>
     <w lemma="additional" pos="j" reg="additional" xml:id="A90113-001-a-1890">additionall</w>
     <w lemma="act" pos="n1" xml:id="A90113-001-a-1900">Act</w>
     <w lemma="for" pos="acp" xml:id="A90113-001-a-1910">for</w>
     <w lemma="the" pos="d" xml:id="A90113-001-a-1920">the</w>
     <w lemma="corporation" pos="n1" xml:id="A90113-001-a-1930">Corporation</w>
     <w lemma="for" pos="acp" xml:id="A90113-001-a-1940">for</w>
     <w lemma="the" pos="d" xml:id="A90113-001-a-1950">the</w>
     <w lemma="poor" pos="j" xml:id="A90113-001-a-1960">Poor</w>
     <w lemma="of" pos="acp" xml:id="A90113-001-a-1970">of</w>
     <w lemma="the" pos="d" xml:id="A90113-001-a-1980">the</w>
     <w lemma="city" pos="n1" xml:id="A90113-001-a-1990">City</w>
     <w lemma="of" pos="acp" xml:id="A90113-001-a-2000">of</w>
     <hi xml:id="A90113-e130">
      <w lemma="London" pos="nn1" xml:id="A90113-001-a-2010">London</w>
      <pc xml:id="A90113-001-a-2020">,</pc>
     </hi>
     <w lemma="without" pos="acp" xml:id="A90113-001-a-2030">without</w>
     <w lemma="which" pos="crq" xml:id="A90113-001-a-2040">which</w>
     <w lemma="the" pos="d" xml:id="A90113-001-a-2050">the</w>
     <w lemma="new" pos="av-j" xml:id="A90113-001-a-2060">newly</w>
     <w lemma="lay" pos="vvn" xml:id="A90113-001-a-2070">laid</w>
     <w lemma="foundation" pos="n1" xml:id="A90113-001-a-2080">foundation</w>
     <w lemma="in" pos="acp" xml:id="A90113-001-a-2090">in</w>
     <w lemma="their" pos="po" xml:id="A90113-001-a-2100">their</w>
     <w lemma="work" pos="n1" xml:id="A90113-001-a-2110">work</w>
     <pc xml:id="A90113-001-a-2120">,</pc>
     <pc join="right" xml:id="A90113-001-a-2130">(</pc>
     <w lemma="in" pos="acp" xml:id="A90113-001-a-2140">in</w>
     <w lemma="which" pos="crq" xml:id="A90113-001-a-2150">which</w>
     <pc join="right" xml:id="A90113-001-a-2160">(</pc>
     <w lemma="by" pos="acp" xml:id="A90113-001-a-2170">by</w>
     <w lemma="the" pos="d" xml:id="A90113-001-a-2180">the</w>
     <w lemma="blessing" pos="n1" xml:id="A90113-001-a-2190">blessing</w>
     <w lemma="of" pos="acp" xml:id="A90113-001-a-2200">of</w>
     <w lemma="God" pos="nn1" xml:id="A90113-001-a-2210">God</w>
     <pc xml:id="A90113-001-a-2220">)</pc>
     <w lemma="they" pos="pns" xml:id="A90113-001-a-2230">they</w>
     <w lemma="have" pos="vvb" xml:id="A90113-001-a-2240">have</w>
     <w lemma="make" pos="vvn" xml:id="A90113-001-a-2250">made</w>
     <w lemma="some" pos="d" xml:id="A90113-001-a-2260">some</w>
     <w lemma="good" pos="j" xml:id="A90113-001-a-2270">good</w>
     <w lemma="progress" pos="n1" reg="progress" xml:id="A90113-001-a-2280">progresse</w>
     <pc xml:id="A90113-001-a-2290">)</pc>
     <w lemma="will" pos="vmb" xml:id="A90113-001-a-2300">will</w>
     <w lemma="be" pos="vvi" xml:id="A90113-001-a-2310">be</w>
     <w lemma="in" pos="acp" xml:id="A90113-001-a-2320">in</w>
     <w lemma="danger" pos="n1" xml:id="A90113-001-a-2330">danger</w>
     <w lemma="to" pos="prt" xml:id="A90113-001-a-2340">to</w>
     <w lemma="be" pos="vvi" xml:id="A90113-001-a-2350">be</w>
     <w lemma="demolish" pos="vvn" xml:id="A90113-001-a-2360">demolished</w>
     <pc unit="sentence" xml:id="A90113-001-a-2370">.</pc>
    </p>
    <p xml:id="A90113-e140">
     <w lemma="these" pos="d" xml:id="A90113-001-a-2380">These</w>
     <w lemma="thing" pos="n2" xml:id="A90113-001-a-2390">things</w>
     <w lemma="be" pos="vvb" xml:id="A90113-001-a-2400">are</w>
     <w lemma="humble" pos="av-j" xml:id="A90113-001-a-2410">humbly</w>
     <w lemma="propose" pos="vvn" xml:id="A90113-001-a-2420">proposed</w>
     <w lemma="unto" pos="acp" xml:id="A90113-001-a-2430">unto</w>
     <w lemma="you" pos="pn" xml:id="A90113-001-a-2440">you</w>
     <w lemma="in" pos="acp" xml:id="A90113-001-a-2450">in</w>
     <w lemma="the" pos="d" xml:id="A90113-001-a-2460">the</w>
     <w lemma="name" pos="n1" xml:id="A90113-001-a-2470">Name</w>
     <w lemma="of" pos="acp" xml:id="A90113-001-a-2480">of</w>
     <w lemma="the" pos="d" xml:id="A90113-001-a-2490">the</w>
     <w lemma="corporation" pos="n1" xml:id="A90113-001-a-2500">Corporation</w>
     <w lemma="aforementioned" pos="j" xml:id="A90113-001-a-2510">aforementioned</w>
     <pc xml:id="A90113-001-a-2520">,</pc>
    </p>
    <closer xml:id="A90113-e150">
     <signed xml:id="A90113-e160">
      <w lemma="by" pos="acp" xml:id="A90113-001-a-2530">By</w>
      <w lemma="your" pos="po" xml:id="A90113-001-a-2540">your</w>
      <w lemma="most" pos="avs-d" xml:id="A90113-001-a-2550">most</w>
      <w lemma="humble" pos="j" xml:id="A90113-001-a-2560">humble</w>
      <w lemma="servant" pos="n1" xml:id="A90113-001-a-2570">Servant</w>
      <w lemma="EDWARD" pos="nn1" xml:id="A90113-001-a-2580">EDWARD</w>
      <w lemma="odle" pos="n1-vg" xml:id="A90113-001-a-2590">ODLING</w>
      <w lemma="ἱατρόσ" pos="n1" xml:id="A90113-001-a-2600">ἱατρόσ</w>
      <w lemma="solicitor" pos="n1" xml:id="A90113-001-a-2610">Solicitor</w>
      <w lemma="for" pos="acp" xml:id="A90113-001-a-2620">for</w>
      <w lemma="the" pos="d" xml:id="A90113-001-a-2630">the</w>
      <w lemma="say" pos="j-vn" xml:id="A90113-001-a-2640">said</w>
      <w lemma="corporation" pos="n1" xml:id="A90113-001-a-2650">Corporation</w>
      <pc unit="sentence" xml:id="A90113-001-a-2660">.</pc>
     </signed>
    </closer>
   </div>
  </body>
 </text>
</TEI>
